import { createStore } from "redux";
import { combineReducers } from "redux-immutable";

import carts from "../pages/carts/store";

export default createStore(
  combineReducers({
    carts,
  })
);
