import { fromJS } from "immutable";
const initData = fromJS({
  tableData: [
    { id: 1, title: "a", num: 3, price: 3.45 },
    { id: 1, title: "a", num: 3, price: 3.45 },
    { id: 1, title: "a", num: 3, price: 3.45 },
    { id: 1, title: "a", num: 3, price: 3.45 },
  ],
});

const reducer = (state = initData, action) => {
  switch (action.type) {
    case "CARTS/ADD":
      return state.updateIn(
        ["tableData", action.payload, "num"],
        (num) => num + 1
      );
    case "CARTS/DEL":
      return state.updateIn(
        ["tableData", action.payload, "num"],
        (num) => num - 1
      );
    default:
      break;
  }
};

export default reducer;
